package com.isa.eventvalidator.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.isa.eventvalidator.R;
import com.isa.eventvalidator.activity.DetailActivity;
import com.isa.eventvalidator.activity.MainActivity;
import com.isa.eventvalidator.entity.Event;
import com.isa.eventvalidator.util.Functions;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ISA on 6/13/2017.
 */

public class EventAdapter extends RecyclerView.Adapter<EventAdapter.EventTileHolder> implements Filterable{
    public List<Event> eventfiltered;
    public List<Event> events;
    private Context context;

    public EventAdapter(Context context, List<Event> events){
        this.context = context;
        this.events = events;
        this.eventfiltered = events;
    }

    @Override
    public EventTileHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View layoutView = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_event,null);
        EventTileHolder rcv = new EventTileHolder(layoutView);
        return rcv;
    }

    @Override
    public void onBindViewHolder(final EventTileHolder holder, final int pos){
        final Event e = events.get(pos);
        if(e.getLogoURL()!=null){
            Functions.glideDefault(context,holder.imgEvent,e.getLogoURL());
        }
        holder.name.setText(e.getEventName());
        if(e.getEventDescription()!=null) {
            holder.desc.setVisibility(View.VISIBLE);
            //holder.desc.setText(e.getEventDescription());
        }else{
            holder.desc.setVisibility(View.GONE);
        }
        holder.location.setText(" "+e.getEventLocation());

        String date;
        String dateStart = Functions.convertSecondToAnyFormat(e.getEventStartTime(),"d");
        String dateEnd = Functions.convertSecondToAnyFormat(e.getEventEndTime(),"d");
        String monthStart = Functions.convertSecondToAnyFormat(e.getEventStartTime(),"MMM");
        String monthEnd = Functions.convertSecondToAnyFormat(e.getEventEndTime(),"MMM");
        String yearStart = Functions.convertSecondToAnyFormat(e.getEventStartTime(),"yyyy");
        String yearEnd = Functions.convertSecondToAnyFormat(e.getEventEndTime(),"yyyy");
        if(dateStart.equals(dateEnd)){ // date 0
            date = Functions.formatDate(e.getEventStartTime(),e.getEventEndTime(),0);
            e.setDate(date);
        }
        else if(!yearStart.equals(yearEnd)) {	  // year 0
            date = Functions.formatDate(e.getEventStartTime(),e.getEventEndTime(),1);
            e.setDate(date);
        }else{								  // year 1
            if(!monthStart.equals(monthEnd)){ // year 1 month 0
                date = Functions.formatDate(e.getEventStartTime(),e.getEventEndTime(),2);
                e.setDate(date);
            }else{							  // year 1 month 1
                date = Functions.formatDate(e.getEventStartTime(),e.getEventEndTime(),3);
                e.setDate(date);
            }
        }
        holder.date.setText(" "+date);
        holder.capacity.setText(e.getSeatCapacity()+" seats");
        holder.participant.setText(e.getNoOfParticipants()+" people");
        holder.attendees.setText(e.getNoOfAttendees()+" seats");
    }

    @Override
    public int getItemCount() {
        return events == null ? 0 :  events.size();
    }

    @Override
    public Filter getFilter() {
        return new Filter() {
            @Override
            protected FilterResults performFiltering(CharSequence charSequence) {
                String charString = charSequence.toString();
                if(charString.isEmpty()){
                    events = eventfiltered;
                }else{
                    ArrayList<Event> filteredList = new ArrayList<>();
                    for(Event p:eventfiltered){
                        if(p.getEventName().toLowerCase().contains(charString)){
                            filteredList.add(p);
                        }
                    }
                    events = filteredList;
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values=events;
                return filterResults;
            }

            @Override
            protected void publishResults(CharSequence charSequence, FilterResults filterResults) {
                events = (ArrayList<Event>)filterResults.values;
                notifyDataSetChanged();
            }
        };
    }

    public class EventTileHolder extends RecyclerView.ViewHolder{
        public TextView name,desc,location,date,capacity,participant,attendees;
        public ImageView imgEvent;
        public EventTileHolder(final View itemView){
            super(itemView);
                imgEvent = (ImageView) itemView.findViewById(R.id.imgEvent);
                name = (TextView)itemView.findViewById(R.id.tvName);
                desc = (TextView)itemView.findViewById(R.id.tvDesc);
                location = (TextView) itemView.findViewById(R.id.tvLocation);
                date = (TextView)itemView.findViewById(R.id.tvDate);
                capacity = (TextView) itemView.findViewById(R.id.tvCapacity);
                attendees = (TextView) itemView.findViewById(R.id.tvAttendees);
                participant = (TextView) itemView.findViewById(R.id.tvParticipants);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    final Event e = events.get(getAdapterPosition());
                    Intent eventDetail = new Intent(v.getContext(), DetailActivity.class);
                    if(e.getLogoURL()!=null) {
                        eventDetail.putExtra("img", e.getLogoURL());
                    }
                    eventDetail.putExtra("nameParticipant","");
                    eventDetail.putExtra("id",e.getEventID());
                    eventDetail.putExtra("name",e.getEventName());
                    eventDetail.putExtra("loc",e.getEventLocation());
                    eventDetail.putExtra("desc",e.getEventDescription());
                    eventDetail.putExtra("date",e.getDate());
                    eventDetail.putExtra("participant",e.getNoOfParticipants());
                    eventDetail.putExtra("attendees",e.getNoOfAttendees());
                    eventDetail.putExtra("capacity",e.getSeatCapacity());
                    v.getContext().startActivity(eventDetail);
                }
            });
        }
    }
}
