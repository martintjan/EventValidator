package com.isa.eventvalidator.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.nfc.Tag;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.ResultPoint;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.isa.eventvalidator.R;
import com.isa.eventvalidator.entity.Event;
import com.isa.eventvalidator.entity.Status;
import com.isa.eventvalidator.util.Converter;
import com.isa.eventvalidator.util.Engine;
import com.isa.eventvalidator.util.Functions;
import com.isa.eventvalidator.util.RequestTemplate;
import com.journeyapps.barcodescanner.BarcodeCallback;
import com.journeyapps.barcodescanner.BarcodeResult;
import com.journeyapps.barcodescanner.CaptureActivity;
import com.journeyapps.barcodescanner.CaptureManager;
import com.journeyapps.barcodescanner.CompoundBarcodeView;
import com.journeyapps.barcodescanner.DecoratedBarcodeView;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by ISA on 6/13/2017.
 */

public class DetailActivity extends AppCompatActivity {
    String eventId, img,name, desc, location, date, nameParticipant="";
    int participants,attendees,capacity;
    ImageView imgEvent;
    TextView tvName,tvNameParticipant, tvDesc, tvSeeMore, tvLocation, tvDate, tvCapacity, tvParticipant, tvAttendees;

    CompoundBarcodeView scanner;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail);

        eventId = getIntent().getStringExtra("id");
        img = getIntent().getStringExtra("img");
        name = getIntent().getStringExtra("name");
        nameParticipant = getIntent().getStringExtra("nameParticipant");
        location = getIntent().getStringExtra("loc");
        desc = getIntent().getStringExtra("desc");
        date = getIntent().getStringExtra("date");
        participants = getIntent().getIntExtra("participant",0);
        attendees = getIntent().getIntExtra("attendees",0);
        capacity = getIntent().getIntExtra("capacity",0);

        imgEvent = (ImageView)findViewById(R.id.imgEvent);
        tvName = (TextView) findViewById(R.id.tvName);
        tvNameParticipant = (TextView)findViewById(R.id.nameParticipant);
        tvLocation = (TextView)findViewById(R.id.tvLocation);
        tvDesc = (TextView) findViewById(R.id.tvDesc);
        tvSeeMore = (TextView) findViewById(R.id.seeMore);
        tvLocation = (TextView) findViewById(R.id.tvLocation);
        tvDate = (TextView) findViewById(R.id.tvDate);
        tvCapacity = (TextView) findViewById(R.id.tvCapacity);
        tvParticipant = (TextView) findViewById(R.id.tvParticipants);
        tvAttendees = (TextView) findViewById(R.id.tvAttendees);

        scanner = (CompoundBarcodeView) findViewById(R.id.scanner);
        scanner.setStatusText("");
        if(ContextCompat.checkSelfPermission(DetailActivity.this, Manifest.permission.CAMERA)== PackageManager.PERMISSION_GRANTED){
            scanner.decodeSingle(callback);
        }else{
            ActivityCompat.requestPermissions((DetailActivity.this),new String[]{Manifest.permission.CAMERA},456);
        }

        /*
        capture = new CaptureManager(this,scanner);
        capture.initializeFromIntent(getIntent(),savedInstanceState);
        capture.decode();
        */

        if(img.isEmpty()){
            imgEvent.setImageResource(R.drawable.ic_user);
        }else{
            Functions.glideDefault(getApplicationContext(),imgEvent,img);
        }

        if(!nameParticipant.equals("")){
            tvNameParticipant.setText(nameParticipant);
        }
        tvName.setText(name);
        tvLocation.setText(" "+location);
        tvDate.setText(" "+date);
        tvParticipant.setText(participants+" people");
        tvAttendees.setText(attendees+" seats");
        tvCapacity.setText(capacity+" seats");

        tvDesc.setVisibility(View.GONE);

        StringBuilder sb = new StringBuilder();
        ArrayList<String> description = new ArrayList<>();
        description.add(desc);
        for(String string : description){
            sb.append(string);
            tvDesc.setText(sb.length()>0?sb.substring(0,sb.length()-1):"");
        }
        tvDesc.post(new Runnable() {
            @Override
            public void run() {
                int a = tvDesc.getLineCount();
                if(a>1){
                    tvSeeMore.setVisibility(View.VISIBLE);
                    tvDesc.setMaxLines(1);
                }
            }
        });
        tvSeeMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvDesc.setMaxLines(Integer.MAX_VALUE);
                tvSeeMore.setVisibility(View.GONE);
            }
        });
    }

    private BarcodeCallback callback = new BarcodeCallback() {
        @Override
        public void barcodeResult(BarcodeResult result) {
            if (result.getText() != null) {
                //scanner.setStatusText(result.getText());
                Log.d("QR Code Result : ",result.getText());

                //String trim[] = result.getText().split(" ",3);
                final String participantID = result.getText();
                final String eventID = eventId;
                Log.d("QR Code Result", "Participant : "+participantID+", Event : "+eventID);
                HashMap<String,String> params = new HashMap<>();
                params.put("eventID", eventID);
                params.put("participantID", participantID);
                Engine.postEvent(DetailActivity.this, params, new RequestTemplate.ServiceCallback() {
                    @Override
                    public void execute(JSONObject obj) {
                        try {
                            if(obj.getString("status").equals("SUCCESS")){
                                final Status status = Converter.convertStatus(obj);
                                Functions.showAlertWithCallback(DetailActivity.this, obj.getString("status"), obj.getString("errorMessages"), new RequestTemplate.ActionCallback() {
                                    @Override
                                    public void execute() {
                                        Intent refresh = getIntent();
                                        refresh.putExtra("nameParticipant",status.getParticipantName());
                                        refresh.putExtra("img", img);
                                        refresh.putExtra("id",eventId);
                                        refresh.putExtra("name",name);
                                        refresh.putExtra("loc",location);
                                        refresh.putExtra("desc",desc);
                                        refresh.putExtra("date",date);
                                        refresh.putExtra("participant",participants);
                                        refresh.putExtra("attendees",status.getEvent().getNoOfAttendees());
                                        refresh.putExtra("capacity",capacity);
                                        startActivity(refresh);
                                        scanner.decodeSingle(callback);
                                        overridePendingTransition(android.R.anim.fade_in,android.R.anim.fade_out);
                                    }
                                });
                            }else{
                                Functions.showAlertWithCallback(DetailActivity.this, obj.getString("status"), obj.getString("errorMessages"), new RequestTemplate.ActionCallback() {
                                    @Override
                                    public void execute() {
                                        scanner.decodeSingle(callback);
                                    }
                                });
                            }
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }

        @Override
        public void possibleResultPoints(List<ResultPoint> resultPoints) {
        }
    };


    @Override
    public void onResume() {
        scanner.resume();
        super.onResume();
    }

    @Override
    public void onPause() {
        scanner.pause();
        super.onPause();
    }
}
