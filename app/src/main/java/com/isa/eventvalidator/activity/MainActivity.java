package com.isa.eventvalidator.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.SearchView;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.isa.eventvalidator.R;
import com.isa.eventvalidator.adapter.EventAdapter;
import com.isa.eventvalidator.component.SmoothRecyclerView;
import com.isa.eventvalidator.entity.Event;
import com.isa.eventvalidator.entity.User;
import com.isa.eventvalidator.util.Converter;
import com.isa.eventvalidator.util.Engine;
import com.isa.eventvalidator.util.RequestTemplate;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    int positionIndex,topView;

    List<Event> allEvents;
    SearchView searchEvent;
    SmoothRecyclerView eventRecycler;
    LinearLayout emptyEvent;
    EventAdapter eventAdapter;
    LinearLayoutManager eventLinear;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        searchEvent = (SearchView)findViewById(R.id.searchEvent);
        searchEvent.setQueryHint("Search event here");
        searchEvent.setIconifiedByDefault(false);

        eventRecycler = (SmoothRecyclerView) findViewById(R.id.recycler_all_event);
        eventLinear = new LinearLayoutManager(this);
        eventRecycler.hasFixedSize();
        eventRecycler.setLayoutManager(eventLinear);

        emptyEvent = (LinearLayout) findViewById(R.id.empty_event);
        allEvents = new ArrayList<>();
        Engine.login(getApplicationContext(), new RequestTemplate.ActionCallback() {
            @Override
            public void execute() {
                Engine.getEvent(getApplicationContext(), new RequestTemplate.ArrayCallback() {
                    @Override
                    public void execute(JSONArray array) {
                        try{
                            for(int i=0;i<array.length();i++){
                                Event event = Converter.convertEvent(array.getJSONObject(i));
                                allEvents.add(event);
                            }
                            if(allEvents.isEmpty()){
                                searchEvent.setVisibility(View.GONE);
                                eventRecycler.setVisibility(View.GONE);
                                emptyEvent.setVisibility(View.VISIBLE);
                            }else{
                                searchEvent.setVisibility(View.VISIBLE);
                                eventRecycler.setVisibility(View.VISIBLE);
                                emptyEvent.setVisibility(View.GONE);
                                eventAdapter = new EventAdapter(getApplicationContext(), allEvents);
                                eventRecycler.setAdapter(eventAdapter);
                                if(positionIndex!= -1) {
                                    eventLinear.scrollToPositionWithOffset(positionIndex, topView);
                                }
                                eventAdapter.notifyDataSetChanged();
                            }

                            searchEvent.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
                                @Override
                                public boolean onQueryTextSubmit(String query) {
                                    return false;
                                }

                                @Override
                                public boolean onQueryTextChange(String newText) {
                                    eventAdapter.getFilter().filter(newText);
                                    return true;
                                }
                            });
                        }catch (JSONException e){
                            e.printStackTrace();
                        }
                    }
                });
            }
        }, null);
    }
    @Override
    protected void onPause() {
        super.onPause();
        positionIndex = eventLinear.findFirstVisibleItemPosition();
        View startView = eventRecycler.getChildAt(0);
        topView = (startView == null)  ? 0 : (startView.getTop() - eventRecycler.getPaddingTop());
    }
}
