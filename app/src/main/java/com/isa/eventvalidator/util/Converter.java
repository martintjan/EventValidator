package com.isa.eventvalidator.util;

import android.content.Context;

import com.google.gson.Gson;
import com.isa.eventvalidator.entity.Event;
import com.isa.eventvalidator.entity.Status;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

/**
 * Created by NAIT ADMIN on 13/06/2016.
 */
public class Converter {
	public static Context context;
	static Gson gson=new Gson();

	public static Event convertEvent(JSONObject obj)throws JSONException{
		return gson.fromJson(obj.toString(), Event.class);
	}
	public static Status convertStatus(JSONObject obj)throws JSONException{
		return gson.fromJson(obj.toString(), Status.class);
	}
	public static Status.Event convertEventStatus(Status.Event event){
		return gson.fromJson(event.toString(),Status.Event.class);
	}

}
