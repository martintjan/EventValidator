package com.isa.eventvalidator.util;

import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.widget.ImageView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.model.GlideUrl;
import com.bumptech.glide.load.model.LazyHeaders;
import com.bumptech.glide.request.target.BitmapImageViewTarget;
import com.isa.eventvalidator.R;
import com.isa.eventvalidator.entity.User;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;
import java.util.Locale;

/**
 * Created by NAIT ADMIN on 24/06/2016.
 */
public class Functions {
	public static void showAlert(Context context,String title, String message){
		AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AppTheme)).create();
		alertDialog.setTitle(title);
		alertDialog.setMessage(message);
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
					}
				});
		alertDialog.show();
	}
	public static void showAlertWithCallback(Context context, String title, String message, final RequestTemplate.ActionCallback callback){
		AlertDialog alertDialog = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.AppTheme)).create();
		alertDialog.setTitle(title);
		if(message.equals("SUCCESS")) {
			alertDialog.setIcon(R.drawable.ic_check);
		}else{
			alertDialog.setIcon(R.drawable.ic_error);
		}
		alertDialog.setMessage(message);
		alertDialog.setButton(AlertDialog.BUTTON_POSITIVE, "OK",
				new DialogInterface.OnClickListener() {
					public void onClick(DialogInterface dialog, int which) {
						dialog.dismiss();
						callback.execute();
					}
				});
		alertDialog.show();
	}
	public static String convertSecondToDate(long second){
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat("d MMM yyyy kk:mm", Locale.US);
		second=second*1000;
		String a = simpleDateFormat.format(second);
		return a;
	}

	public static String convertSecondToAnyFormat(long second, String format){
		SimpleDateFormat simpleDateFormat=new SimpleDateFormat(format, Locale.US);
		//second=second*1000;
		String a = simpleDateFormat.format(second);
		return a;
	}
	public static String getTimeAgo(long time) {
		long longtime = time *1000;
		final int SECOND_MILLIS = 1000;
		final int MINUTE_MILLIS = 60 * SECOND_MILLIS;
		final int HOUR_MILLIS = 60 * MINUTE_MILLIS;
		if (time < 1000000000000L) {
			// if timestamp given in seconds, convert to millis
			time *= 1000;
		}

		long now = System.currentTimeMillis();
		if (time > now || time <= 0) {
			return null;
		}

		// TODO: localize
		final long diff = now - time;
		if (diff < MINUTE_MILLIS) {
			return "just now";
		} else if (diff < 2 * MINUTE_MILLIS) {
			return "a minute ago";
		} else if (diff < 50 * MINUTE_MILLIS) {
			return diff / MINUTE_MILLIS + " minutes ago";
		} else if (diff < 120 * MINUTE_MILLIS) {
			return "an hour ago";
		} else if (diff < 24 * HOUR_MILLIS) {
			return diff / HOUR_MILLIS + " hours ago";
		} else if (diff < 48 * HOUR_MILLIS) {
			return "yesterday";
		} else {
			return convertSecondToAnyFormat(longtime,"MMM, d yyyy");
		}
	}

	public static List<Object> sortingContact(List<Object> array){
		Collections.sort(array,
				new Comparator<Object>()
				{
					public int compare(Object f1, Object f2)
					{
						return f1.toString().compareToIgnoreCase(f2.toString());
					}
				});
		return array;
	}
	public static int DifferentDays(final String start, final String end){
		int result;
		Date d1 = null;
		Date d2 = null;
		final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("EEEE, dd MMM yyyy", Locale.US);
		try {
			d1 = simpleDateFormat.parse(start);
			d2 = simpleDateFormat.parse(end);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		int diffInDay = (int) ((d2.getTime() - d1.getTime()) / (1000 * 60 * 60 * 24));
		return result=diffInDay;
	}

	public static void glideDefault(final Context context, ImageView imageView, String url){
			GlideUrl glideUrl = new GlideUrl(url, new LazyHeaders.Builder()
					.addHeader("Authorization", "Bearer " + User.getUser().getAccess_token())
					.addHeader("Accept", "image/png")
					.build());

			Glide.with(context)
					.load(glideUrl)
					.crossFade()
					.diskCacheStrategy(DiskCacheStrategy.RESULT)
                    .override(500,450)
					.fitCenter()
					.into(imageView);
	}
	public static String formatDate(final long timestart, final long timeend, final int condition){
		String date="",dateStart="",dateEnd="";
			switch (condition){
				case 0 :
					dateStart = Functions.convertSecondToAnyFormat(timestart,"k:mm");
					dateEnd = Functions.convertSecondToAnyFormat(timeend,"k:mm  MMM d, yyyy");
					date = dateStart+" - "+dateEnd;
					break;
				case 1 :
					dateStart = Functions.convertSecondToAnyFormat(timestart,"MMM d, yyyy");
					dateEnd = Functions.convertSecondToAnyFormat(timeend,"MMM d, yyyy");
					date = dateStart+" - "+dateEnd;
					break;
				case 2 :
					dateStart = Functions.convertSecondToAnyFormat(timestart,"MMM d");
					dateEnd = Functions.convertSecondToAnyFormat(timeend,"MMM d, yyyy");
					date = dateStart+" - "+dateEnd;
					break;
				case 3 :
					dateStart = Functions.convertSecondToAnyFormat(timestart,"MMM d");
					dateEnd = Functions.convertSecondToAnyFormat(timeend,"d, yyyy");
					date = dateStart+" - "+dateEnd;
					break;
			}
		return date;
	}
	public static boolean isPackageInstalled(String packagename, PackageManager packageManager) {
		try {
			packageManager.getPackageInfo(packagename, 0);
			return true;
		} catch (PackageManager.NameNotFoundException e) {
			return false;
		}
	}
}
