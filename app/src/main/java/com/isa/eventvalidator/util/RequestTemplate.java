package com.isa.eventvalidator.util;

import android.content.Context;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NoConnectionError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.isa.eventvalidator.entity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by NAIT ADMIN on 06/06/2016.
 */
public class RequestTemplate {
	public interface ActionCallback{		void execute();}
	public interface StringCallback{		void execute(String string);}
	public interface ServiceCallback {		void execute(JSONObject obj);}
	public interface ErrorCallback {		void execute(JSONObject error);}
	public interface ErrorActionCallback{	void execute();}
	public interface ArrayCallback{			void execute(JSONArray array);}

	public static void OAuth(final Context context, String url, final Map<String,String> params,
							 final ServiceCallback callback, final ErrorActionCallback errorCallback){
		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				try {
					callback.execute(new JSONObject(response));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				if(error instanceof TimeoutError || error instanceof NoConnectionError){
					Toast.makeText(context,"Connection Timeout",Toast.LENGTH_SHORT).show();
				}else if (error instanceof ServerError){
					if (errorCallback!=null){
						errorCallback.execute();
					}
				}
			}
		}){
			@Override
			public Map<String,String> getHeaders() throws AuthFailureError {
				Map<String,String> params = new HashMap<String,String>();
				params.put("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
				return params;
			}

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				return params;
			}

			@Override
			public String getBodyContentType() {
				return "application/x-www-form-urlencoded; charset=utf-8";
			}

		};
		VolleySingleton.getInstance(context).addToRequestQueue(request);
	}

	public static void POSTJsonRequest(final Context context, final String url, final JSONObject params,
	                                    final ServiceCallback callback, final ErrorCallback errorCallback){
		final JsonObjectRequestWithNull jsonObjectRequest = new JsonObjectRequestWithNull(Request.Method.POST, url, params, new Response.Listener<JSONObject>() {
			@Override
			public void onResponse(JSONObject response) {
				callback.execute(response);
			}
		},new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				if(error instanceof TimeoutError || error instanceof NoConnectionError){
					Toast.makeText(context,"You have no connection",Toast.LENGTH_SHORT).show();
				}else {
					if (errorCallback!=null){
						try {
							errorCallback.execute(new JSONObject(error.getMessage()));
						} catch (JSONException e) {
							e.printStackTrace();
						}
					}
				}
			}
		}){
			@Override
			public Map<String,String> getHeaders() throws AuthFailureError {
				Map<String,String> params = new HashMap<String,String>();
				params.put("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
				params.put("Authorization", "Bearer " + User.getUser().getAccess_token());
				return params;
			}
		};
		VolleySingleton.getInstance(context).addToRequestQueue(jsonObjectRequest);
		jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000,1,1.0f));
	}

	public static void POSTStringRequest(final Context context, String url, final Map<String,String> params,
							 final ServiceCallback callback, final ErrorActionCallback errorCallback){
		StringRequest request = new StringRequest(Request.Method.POST, url, new Response.Listener<String>() {
			@Override
			public void onResponse(String response) {
				try {
					callback.execute(new JSONObject(response));
				} catch (JSONException e) {
					e.printStackTrace();
				}
			}
		}, new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				if(error instanceof TimeoutError || error instanceof NoConnectionError){
					Toast.makeText(context,"Connection Timeout",Toast.LENGTH_SHORT).show();
				}else if (error instanceof ServerError){
					if (errorCallback!=null){
						errorCallback.execute();
					}
				}
			}
		}){
			@Override
			public Map<String,String> getHeaders() throws AuthFailureError {
				Map<String,String> params = new HashMap<String,String>();
				params.put("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
				params.put("Authorization", "Bearer " + User.getUser().getAccess_token());
				return params;
			}

			@Override
			protected Map<String, String> getParams() throws AuthFailureError {
				return params;
			}

			@Override
			public String getBodyContentType() {
				return "application/x-www-form-urlencoded; charset=utf-8";
			}

		};
		VolleySingleton.getInstance(context).addToRequestQueue(request);
	}

	public static void GETJsonArrayRequest(final Context context, final String url,
										   final ArrayCallback callback, final ActionCallback errorCallback){
		final JsonArrayRequest arrayRequest = new JsonArrayRequest(url, new Response.Listener<JSONArray>() {
			@Override
			public void onResponse(JSONArray response) {
				callback.execute(response);
			}
		},new Response.ErrorListener() {
			@Override
			public void onErrorResponse(VolleyError error) {
				if(error instanceof TimeoutError || error instanceof NoConnectionError){
					Toast.makeText(context,"You have no connection",Toast.LENGTH_SHORT).show();
				}else {
						if (error.networkResponse!=null){
							if (errorCallback!=null){
								errorCallback.execute();
							}
					}
				}
			}
		}){
			@Override
			public Map<String,String> getHeaders() throws AuthFailureError {
				Map<String,String> params = new HashMap<String,String>();
				params.put("Authorization", "Bearer " + User.getUser().getAccess_token());
				return params;
			}
			@Override
			public String getBodyContentType() {
				return "application/x-www-form-urlencoded; charset=utf-8";
			}
		};
		VolleySingleton.getInstance(context).addToRequestQueue(arrayRequest);
		arrayRequest.setRetryPolicy(new DefaultRetryPolicy(20 * 1000,1,1.0f));
	}
}
