package com.isa.eventvalidator.util;

import android.content.Context;
import android.content.Intent;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.isa.eventvalidator.R;
import com.isa.eventvalidator.activity.MainActivity;
import com.isa.eventvalidator.entity.User;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by NAIT ADMIN on 07/06/2016.
 */
public class Engine {
	public static void login(final Context context,
	                         final RequestTemplate.ActionCallback callback, final RequestTemplate.ActionCallback errorCallback){
		String url="https://na50.salesforce.com/services/oauth2/token";
		HashMap<String,String> params = new HashMap<>();
		params.put("username",context.getString(R.string.username));
		params.put("password",context.getString(R.string.password));
		params.put("grant_type","password");
		params.put("client_id",context.getString(R.string.client_id));
		params.put("client_secret",context.getString(R.string.client_secret));

		RequestTemplate.OAuth(context, url, params, new RequestTemplate.ServiceCallback() {
			@Override
			public void execute(JSONObject obj) {
				try {
					User.getUser().setAccess_token(obj.getString("access_token"));
					if (callback != null) {
						callback.execute();
					}
				}catch (JSONException e){
					e.printStackTrace();
				}
			}
		}, new RequestTemplate.ErrorActionCallback() {
			@Override
			public void execute() {
				Toast.makeText(context, "ERROR",Toast.LENGTH_SHORT).show();
			}
		});
	}
	public static void getEvent(final Context context,
	                                final RequestTemplate.ArrayCallback callback){
		String url = "https://na50.salesforce.com/services/apexrest/eventRegistration/";
		RequestTemplate.GETJsonArrayRequest(context, url, new RequestTemplate.ArrayCallback() {
				@Override
				public void execute(JSONArray array) {
					if (array!=null){}
					if (callback!=null){
						callback.execute(array);
					}
				}
			},null);
		}
	public static void postEvent(final Context context, final HashMap<String,String> params,
								final RequestTemplate.ServiceCallback callback){
		String url = "https://na50.salesforce.com/services/apexrest/eventRegistration/";
		RequestTemplate.POSTStringRequest(context, url, params, new RequestTemplate.ServiceCallback() {
			@Override
			public void execute(JSONObject obj) {
				if (callback != null) {
					callback.execute(obj);
				}
			}
		},null);
	}
}
