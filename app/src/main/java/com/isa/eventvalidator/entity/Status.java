package com.isa.eventvalidator.entity;

/**
 * Created by ISA on 6/15/2017.
 */

public class Status {
    String status;
    String participantName;
    Event event;
    String errorMessages;

    public String getStatus() {                                 return status;}
    public void setStatus(String status) {                      this.status = status;}

    public String getParticipantName() {                        return participantName;}
    public void setParticipantName(String participantName) {    this.participantName = participantName;}

    public Event getEvent() {                                   return event;}
    public void setEvent(Event event) {                         this.event = event;}

    public String getErrorMessages() {                          return errorMessages;}
    public void setErrorMessages(String errorMessages) {        this.errorMessages = errorMessages;}

    public class Event{
        int seatCapacity;
        int noOfParticipants;
        int noOfAttendees;
        String logoURL;
        String eventStatus;
        long eventStartTime;
        String eventName;
        String eventID;
        long eventEndTime;
        String eventLocation;
        String eventDescription;
        String date;

        public String getDate(){return date;}
        public void setDate(String date){this.date = date;}

        public String getLogoURL(){return logoURL;}
        public void setLogoURL(String logoURL){this.logoURL = logoURL;}

        public int getSeatCapacity() {
            return seatCapacity;
        }
        public void setSeatCapacity(int seatCapacity) {
            this.seatCapacity = seatCapacity;
        }

        public int getNoOfParticipants() {
            return noOfParticipants;
        }
        public void setNoOfParticipants(int noOfParticipants) {
            this.noOfParticipants = noOfParticipants;
        }

        public int getNoOfAttendees() {
            return noOfAttendees;
        }
        public void setNoOfAttendees(int noOfAttendees) {
            this.noOfAttendees = noOfAttendees;
        }

        public String getEventStatus() {
            return eventStatus;
        }
        public void setEventStatus(String eventStatus) {
            this.eventStatus = eventStatus;
        }

        public long getEventStartTime() {
            return eventStartTime;
        }
        public void setEventStartTime(long eventStartTime) {
            this.eventStartTime = eventStartTime;
        }

        public String getEventName() {
            return eventName;
        }
        public void setEventName(String eventName) {
            this.eventName = eventName;
        }

        public String getEventID() {
            return eventID;
        }
        public void setEventID(String eventID) {
            this.eventID = eventID;
        }

        public long getEventEndTime() {
            return eventEndTime;
        }
        public void setEventEndTime(long eventEndTime) {
            this.eventEndTime = eventEndTime;
        }

        public String getEventLocation() {
            return eventLocation;
        }
        public void setEventLocation(String eventLocation) {
            this.eventLocation = eventLocation;
        }

        public String getEventDescription() {
            return eventDescription;
        }
        public void setEventDescription(String eventDescription) {
            this.eventDescription = eventDescription;
        }
    }
}
