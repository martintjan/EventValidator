package com.isa.eventvalidator.entity;

import android.graphics.Bitmap;

/**
 * Created by NAIT ADMIN on 07/06/2016.
 */
public class User {
	private static User u;
	private String id;
	private String access_token;

	private User(){
		access_token="";
		id="";
	}

	public static synchronized User getUser() {
		if (u == null) {
			u = new User();
		}
		return u;
	}

	public String getID() {
															return id;
	}
	public void setID(String id) {
			this.id = id;
	}

	public String getAccess_token() {
		return access_token;
	}
	public void setAccess_token(String access_token) {
		this.access_token = access_token;
	}

	}
